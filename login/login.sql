CREATE TABLE member(
 num_id int not null primary key auto_increment,
 id varchar(15) not null,
 pass varchar(30) not null,
 name varchar(14) not null,
 gender varchar(15) not null,
 birth1 int(6) not null,
 birth2 int(6) not null,
 birth3 int(6) not null,
 regdate datetime not null
);
