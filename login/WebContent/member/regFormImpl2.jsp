<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<script type="text/javascript">
	function regChk() {
		var id = document.form.id.value;
		var pass = document.form.pass.value;
		var name = document.form.name.value;
		var regx = /^[a-zA-Z0-9]*$/;
		
		// 아이디 체크
		if (id.length == 0 || id == null) {
			alert("아이디를 입력하십시오");
			return false;}
		if (!regx.test(id)) {
			alert("아이디는 영어, 숫자만 입력가능합니다.");
			document.form.id.focus();
			return false;}
		
		// 비밀번호 체크
		if (pass.length < 6 || pass == null) {
			alert("비밀번호를 입력하십시오(6글자이상)");
			return false;}

		// 이름 체크
		if (name.length==0 || name == null) {
			alert("이름을 입력하십시오");
			return false;}

		document.form.submit();
	}

	function regCancel() {
		location.href = "../loginForm.jsp";
	}
</script>
<style>
body {
    background-image: url(image2/joinbkg.png);
    background-size: cover;
    background-repeat: no-repeat;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>회원가입 창</title>
</head>
<body >
<center>
	<form action="regFormProc.jsp" name="form" method="post">
<br><br><br><br><br>
		<b>개인정보입력</b> <br> <br> <b>ID<b></b> : <input type="text"
							size="10" maxlength="15" name="id"><br> <br>
						<b>비밀번호 : </b><input type="password" size="15" maxlength="20" name="pass"><br>
						<br><b>이름 : </b> <input type="text" size="13" maxlength="12"
							name="name"><br> <br><b> 성별 :</b> <input type="radio"
							name="gender" value="남자" checked="checked">
						<b>남자</b>&nbsp;&nbsp; <input type="radio" name="gender" value="여자">
						<b>여자</b><br> <br><b> 생년월일 : </b> <select name="birth1">
							<%
								for (int i = 2013; i >= 1900; i--) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
							%>
						</select><b>년</b>&nbsp; <select name="birth2">
							<%
								for (int i = 1; i <= 12; i++) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
							%>
						</select><b>월</b>&nbsp;&nbsp;<select name="birth3">
							<%
								for (int i = 1; i <= 31; i++) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
							%>
						</select><b>일</b><br> <br> <br>
				<br> <input type="button" value="가입신청"onclick="regChk()">&nbsp; 
				<input type="reset" value="다시입력">&nbsp;
					<input type="button" value="취소" onclick="regCancel()">
	</form>
	</center>
</body>
</html>