<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ page import="my.member.MemberDao, my.member.MemberVo, my.game.GameDao, my.game.GameVo" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%!
	int flag=0;
	int prior=1;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<SCRIPT LANGUAGE="JavaScript">
<!--
function go_url(a){ 
	if(a==0)		location.href="index.jsp";
	else if(a==1)	location.href="game.jsp";
	else if(a==2)	location.href="GameEnd.jsp";
}
-->
</SCRIPT>
<title> WAIT A MOMENT!! </title>
</head>
<body background = "image/gamescreen.jpg">

	<h1 align = "center">
	<font color="blue" size = "5">
	상대방을 기다리고 있습니다. 잠시만 기다려 주세요.
	</font>

	<br>
	<font color = "blue" size = "5">
	게임 방법
	</font>
	</h1>
	<h3 align = "center">
	<textarea style = "background-image:url('image/bg03.jpg'); background-repeat:no-repeat; color:#999999; overflow:hidden;"name = "rule" readonly rows="15" cols="100">

	결, 합 게임의 규칙을 알려드리겠습니다.
	규칙은 다음과 같습니다. 
	01. 각각 도형, 도형색, 배경색이 다른 27장의 카드가 사용됩니다.
	02. 공개되는 9개의 카드 중 합이 되는 3장의 카드를 누르셔야 합니다.
	03. 합이란 속성이 모두 같거나 다른 그림들로 이루어진 3장의 카드를 말합니다.
	04. 10라운드 동안 진행되며 각 플레이어 마다 10초의 시간이 주어집니다.
	05. 시간안에 합을 찾으셨으면 버튼을 누르고 합을 눌러주세요.
	06. 10초안에 못찾을 경우에는 상대방에게 턴이 넘어갑니다. 
		합이 없이 6턴 턴이 넘어가면 라운드가 종료됩니다.
	07. 합이 맞을경우 1점, 아닐경우 -1점이 됩니다. 
	08. 합이 없다고 판단하셨을 경우 결을 누르실수 있습니다.
	09. 결이 맞을 경우 라운드가 종료가 되며 3점, 아닐경우에는 턴이 넘어가고 -1점이 됩니다.
	10. 더 많은 점수를 얻은 플레이어가 최종 승자가 됩니다.
	</textarea>
	<br>
	<br>


	</h3>
<%	
	GameDao gdao = new GameDao();
	GameVo gvo1 = gdao.selectClient();
	flag = gvo1.getNum();  
	int end = gvo1.getEnd();
	
	MemberDao mdao = new MemberDao();
	String id = (String)session.getAttribute("loginId");
	MemberVo mvo = mdao.selectClient(id);
	prior = mvo.getPriority();  
	
 	out.println(flag%2+" "+prior%2);
 	
 	if(end==1){
		%>
		<script type="text/javascript">
			window.onload=function(){
				go_url(2);		// go to gameEnd page 
			}	
		</script>
		<%
	}
 	
 	else if(flag%2 == prior%2){
		System.out.println(1+" "+prior+" "+flag);
		%>
		<script type="text/javascript">
			window.onload=function(){
					go_url(1);
			}	
		</script>	 	
		<%
	}
	else{
		System.out.println(2+" "+prior+" "+flag);
		%>
			<SCRIPT LANGUAGE="JavaScript">
				window.onload=function(){
					setTimeout('go_url(0)',1000);
				}
			</SCRIPT> 	
		<%
	}

%>
</body>
</html>