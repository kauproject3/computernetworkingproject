<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% request.setCharacterEncoding("euc-kr"); %>
<%@ page import="my.member.MemberDao, my.member.MemberVo, my.game.GameDao, my.game.GameVo" %>
<%!
	//----------------------------------------------//
	// Generate image ID randomly
	// name and tag array just follow derived from that 
	
     MemberDao mdao = new MemberDao();
     GameDao gdao = new GameDao();	
   
     
	int imgId[] = new int[9];
	String imgName[] = new String[9];
	int existTag[] = new int[27];
	boolean isSet = false;
	public void imgRand(){
		for(int h=0;h<27;h++){
			existTag[h]=0;
		}
		for(int i=0;i<9;i++){
			do{
				imgId[i] =(int)(Math.random()*27);
			}while(existTag[imgId[i]]!=0);
			existTag[imgId[i]]=1;
			imgId[i] =(1+imgId[i]/9)*100+(1+(imgId[i]%9)/3)*10+(imgId[i]%9)%3+1;			
		}
		for(int j=0;j<9;j++){
			imgName[j] =String.format
	("\"background:URL(image/%d.png);border:0px;width:150px; height:150px;cursor:pointer;\"",imgId[j]);
		}										
		gdao.updateImg_id(imgId);			
	}
	
	public void getImage(){
		GameVo gvo2 = gdao.selectImg_id();
		imgId[0]=gvo2.getId1();
		imgId[1]=gvo2.getId2();
		imgId[2]=gvo2.getId3();
		imgId[3]=gvo2.getId4();
		imgId[4]=gvo2.getId5();
		imgId[5]=gvo2.getId6();
		imgId[6]=gvo2.getId7();
		imgId[7]=gvo2.getId8();
		imgId[8]=gvo2.getId9();
		for(int j=0;j<9;j++){
			imgName[j] =String.format
	("\"background:URL(image/%d.png);border:0px;width:150px; height:150px;cursor:pointer;\"",imgId[j]);
		}					
	}
	int table[] = new int[6];
	int idx;	// 정답 테이블에 저장할 때 사용될 inde
	int numA;	// 정답의 갯수
	int sum=0;	// ID의 합의 값 -> 정답 판별의 기준
	// metod answerTable()

 	public void answerTable(){
 
 		numA=0;
 
 		idx=0;
	for(int i=0;i<9;i++){			// 모든 84가지(9개 중 3개) 조합을 검사 한다.
  			for(int j=i+1;j<8;j++){
 				for(int k=j+1;k<9;k++){
				sum=imgId[i]+imgId[j]+imgId[k];		// ID 더함
				// ID의 합의 모든 자리수가 3으로 나뉘는 경우에 한하여 정답이다.
				if((sum/100)%3==0&&(sum/10)%3==0&&sum%3==0){	
					table[idx++] = 100*i+10*j+k;	// 정답 저장, index 증가
					numA++;							// 정답의 수 증가

 					}

 				}

 			}

 		} 

 	}
	//----------------------------------------------//
	//----------------------------------------------//
	
	//----------------------------------------------//
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
	<SCRIPT LANGUAGE="JavaScript">
	<!--
		var Bpick=[0,0,0];
		var rear =0;
		var left = 10;
		var answer = 0;
		var overlap;

		function pick(BN){	// Button pick을 배열에 저장하는 메서드
			overlap = false;	// 중복이 없다고 초기화 해준다.
			for(i=0;i<3;i++) if(BN==Bpick[i]) overlap=true;		// 검사해서 중복이 있으면 true
			if(!overlap){		// 중복이 없다면
				Bpick[rear] = BN;	// 가장 마지막에 button num을 저장한다.
				rear = (rear+1)%3;	// 원형으로 돌아가면서 저장
			}
		}
		
		function reset(){
			for(i=0;i<3;i++) Bpick[i]=0;
			rear=0;
		}
		
		var idx;
		// method reArrange()
		function reArrange(){	// pick한 배열을 작은 순서대로 배열하여 3자리 숫자로 저장하는 메서드
			idx = 0;				// 배열에서 가장 작은 요소의 index 
			for(i=1;i<3;i++) if(Bpick[idx]>Bpick[i]) idx=i;	// 
			if(Bpick[(idx+1)%3]>Bpick[(idx+2)%3]){		// 나머지 두 요소 중 작은 것이 앞으로 가도록 정수 저장
				answer=Bpick[idx]*100+Bpick[(idx+2)%3]*10+Bpick[(idx+1)%3];
			}
			else{
				answer=Bpick[idx]*100+Bpick[(idx+1)%3]*10+Bpick[(idx+2)%3];
			}
		}

		function realTimeout(){
			document.rtcForm.rtcInput.value = left;
			document.rtcForm.c1.value = Bpick[0];
			document.rtcForm.c2.value = Bpick[1];
			document.rtcForm.c3.value = Bpick[2];
			reArrange();									// 선택한 버튼을 정렬하여 정수 값 얻는다.
			if(left<0){			// 시간이 끝난 경우
				document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
				document.sendForm.checkn.value = 2;
			
				document.sendForm.action = "tempWait.jsp";		// post할 위치 지정
				document.sendForm.submit();					// post!!
			}
			left=left-0.1;
			setTimeout("realTimeout()", 100);
		}

		  
		  function setList(a) {
				if(a==0){
				  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
				  	document.sendForm.checkn.value = a;
					
					document.sendForm.action = "tempWait.jsp";		// post할 위치 지정				
					document.sendForm.submit();	
					}
					else if(a==1){
					  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
					  	document.sendForm.checkn.value = a;
					  
						document.sendForm.action = "tempWait.jsp";		// post할 위치 지정
						
						
						
						
						document.sendForm.submit();	
						}
					else if(a==2){
					  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
					  	document.sendForm.checkn.value = a;
	
						document.sendForm.action = "tempWait.jsp";		// post할 위치 지정
						document.sendForm.submit();	
						}

		 }

	-->
	</SCRIPT>
<title>Insert title here</title>
<style type="text/css">
#wrap{margin:0 auto; width:1600px; height:900px; background-image:url(C:\\clean.png);background-size: cover;background-repeat: no-repeat;}
#header{width:1500px; height:50px; margin-left:50px; margin-bottom:0px; }
#middle{width:1600px; float:left;}
#sidebar1{width:50px;height:800px; float:left; margin-left:50px; margin-right:0px;}
#content{margin:0 auto; width:1000px; height:800px;float:left;}
#sidebar2{width:450px;height:800px; float:left; margin-right:50px; margin-right:0px;}
#footer{width:1500px; height:50px; float:left; margin-left:50px; margin-top:0px;}
</style>
</head>
<body onload="realTimeout()"background="image/clean.png">
<body>
<div id="wrap">
   <div id="header"></div>
   <div id="middle">
       <div id="sidebar1"></div>
       <div id="content">
       <center>
	
	 
	<%
    String id = (String)session.getAttribute("loginId");
    

   	
	GameVo gvo2 = gdao.selectImg_id();
	isSet = gvo2.isSet();
	if(!isSet) 	imgRand();
	else 		getImage();%>

	<%answerTable(); %>
	<button type="button" title="test" style=<%=imgName[0]%> onclick="pick(1)"></button>
	<button type="button" title="test" style=<%=imgName[1]%> onclick="pick(2)"></button>
	<button type="button" title="test" style=<%=imgName[2]%> onclick="pick(3)"></button><br>
	<button type="button" title="test" style=<%=imgName[3]%> onclick="pick(4)"></button>
	<button type="button" title="test" style=<%=imgName[4]%> onclick="pick(5)"></button>
	<button type="button" title="test" style=<%=imgName[5]%> onclick="pick(6)"></button><br>
	<button type="button" title="test" style=<%=imgName[6]%> onclick="pick(7)"></button>
	<button type="button" title="test" style=<%=imgName[7]%> onclick="pick(8)"></button>
	<button type="button" title="test" style=<%=imgName[8]%> onclick="pick(9)"></button><br><br>
	
<form name="rtcForm">
<input type="text" name="rtcInput" size="2" readonly="readonly" /><p>
<input type="text" name="c1" size="1" readonly="readonly"/>
<input type="text" name="c2" size="1" readonly="readonly"/>
<input type="text" name="c3" size="1" readonly="readonly"/>
</form>

	<!-- post할 form -->
	<form name="sendForm" method="post">
	<input type="hidden" name=Ainput value=''/>	
	<input type="hidden" name=checkn value=''/>			<!-- pick한 정답과 정답 테이블 post -->
	<input type="hidden" name="at0" value=<%=table[0]%>/>
	<input type="hidden" name="at1" value=<%=table[1]%>/>
	<input type="hidden" name="at2" value=<%=table[2]%>/>
	<input type="hidden" name="at3" value=<%=table[3]%>/>
	<input type="hidden" name="at4" value=<%=table[4]%>/>
	<input type="hidden" name="at5" value=<%=table[5]%>/>
	</form>
	<%=table[0] %>
	<%=table[1] %>
	<%=table[2] %>
	<%=table[3] %>
	<%=table[4] %>
	<%=table[5] %>
	
 
</center>
       </div>
       <div id="sidebar2">
       <center>
       <img src="image/Reset.png"width="150"height="50"align="middle" onclick="reset()">
       <br><br>
 
 	<form name="myFrm">
 	<img src="image/gyul.png"width="150"height="50"align="middle"onclick="setList(0)">
 	<br><br>	
	 <img src="image/hap.png"width="150"height="50"align="middle"onclick="setList(1)">
	 <br>
	 <br>
 	<img src="image/Pass.png"width="150"height="50"align="middle"onclick="setList(2)">
 	<br>
 	<br>
 </form>
 </center>
       </div>
   </div>
   <div id="footer"></div>
</div>
</body>
</html>