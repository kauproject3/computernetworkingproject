package my.game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import my.util.ConnUtil;
import java.sql.ResultSet;

public class GameDao {

 public int insertImg_id(GameVo vo){
  int rst = 0;
  Connection conn = null;
  PreparedStatement ps = null;
  try{
   conn = ConnUtil.getConnection2();
   String sql = "insert into img_id values(null,?,?,?,?,?,?,?,?,?,sysdate())";
   ps = conn.prepareStatement(sql);
   ps.setInt(1, vo.getId1());
   ps.setInt(2, vo.getId2());
   ps.setInt(3, vo.getId3());
   ps.setInt(4, vo.getId4());
   ps.setInt(5, vo.getId5());
   ps.setInt(6, vo.getId6());
   ps.setInt(7, vo.getId7());
   ps.setInt(8, vo.getId8());
   ps.setInt(9, vo.getId9());
   rst = ps.executeUpdate();
  }catch(Exception e){
   e.printStackTrace();
  }finally{
   ConnUtil.close(ps, conn);
  }
  return rst;
 }
 
 public int insertCurrent(GameVo vo){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "insert into current values(null,?,?,?,?,?,?,sysdate())";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, vo.getCurrent1());
	   ps.setInt(2, vo.getCurrent2());
	   ps.setInt(3, vo.getCurrent3());
	   ps.setInt(4, vo.getCurrent4());
	   ps.setInt(5, vo.getCurrent5());
	   ps.setInt(6, vo.getCurrent6());
	   rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public GameVo selectCurrent(){
	  GameVo vo = new GameVo();
	  Connection conn = null;
	  int code=1;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "select * from current where code=?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, code);
	   rs = ps.executeQuery();
	   if(rs.next()){
		  vo.setCurrent1(rs.getInt("current1"));
		  vo.setCurrent2(rs.getInt("current2"));
		  vo.setCurrent3(rs.getInt("current3"));
		  vo.setCurrent4(rs.getInt("current4"));
		  vo.setCurrent5(rs.getInt("current5"));
		  vo.setCurrent6(rs.getInt("current6"));
	   }
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return vo;
	 }
 
 
 public GameVo selectImg_id(){
	  GameVo vo = new GameVo();
	  Connection conn = null;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "select * from img_id";
	   ps = conn.prepareStatement(sql);
	   rs = ps.executeQuery();
	   if(rs.next()){
		  vo.setId1(rs.getInt("id1"));
		  vo.setId2(rs.getInt("id2"));
		  vo.setId3(rs.getInt("id3"));
		  vo.setId4(rs.getInt("id4"));
		  vo.setId5(rs.getInt("id5"));
		  vo.setId6(rs.getInt("id6"));
		  vo.setId7(rs.getInt("id7"));
		  vo.setId8(rs.getInt("id8"));
		  vo.setId9(rs.getInt("id9"));
		  vo.setSet(rs.getBoolean("isset"));
	   }
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return vo;
	 }
 
 public GameVo selectClient(){
	  GameVo vo = new GameVo();
	  Connection conn = null;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "select * from client";
	   ps = conn.prepareStatement(sql);
	   rs = ps.executeQuery();
	   if(rs.next()){
		  vo.setNum(rs.getInt("num"));
		  vo.setEnd(rs.getInt("end"));
		  vo.setScorea(rs.getInt("scorea"));
		  vo.setScoreb(rs.getInt("scoreb"));
	   }
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return vo;
	 }
 
 
 
 
 public int updateCurrent(int[] current){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  int code = 1;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update current set current1=?,current2=?,current3=?,current4=?,current5=?,current6=? where code = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, current[0]);
	   ps.setInt(2, current[1]);
	   ps.setInt(3, current[2]);
	   ps.setInt(4, current[3]);
	   ps.setInt(5, current[4]);
	   ps.setInt(6, current[5]);
	   ps.setInt(7, code);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }

 public int updateImg_id(int[] img_id){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update img_id set id1=?,id2=?,id3=?,id4=?,id5=?,id6=?,id7=?,id8=?,id9=?,isset=true";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, img_id[0]);
	   ps.setInt(2, img_id[1]);
	   ps.setInt(3, img_id[2]);
	   ps.setInt(4, img_id[3]);
	   ps.setInt(5, img_id[4]);
	   ps.setInt(6, img_id[5]);
	   ps.setInt(7, img_id[6]);
	   ps.setInt(8, img_id[7]);
	   ps.setInt(9, img_id[8]);
	  	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 } 
 
 public int initImg_id(){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update img_id set id1=0,id2=0,id3=0,id4=0,id5=0,id6=0,id7=0,id8=0,id9=0,isset=false";
	   ps = conn.prepareStatement(sql);
	  	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 } 
 public int initCurrent(){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  int code = 1;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update current set current1=0,current2=0,current3=0,current4=0,current5=0,current6=0 where code = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, code);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int initNum(){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;

	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update client set num=1,end=0,scorea=0,scoreb=0";
	   ps = conn.prepareStatement(sql);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int updateClienrt(int num){
	  int rst = 0;
	  Connection conn = null;
	  
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update client set num=?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, num);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int updateEnd(int end){
	  int rst = 0;
	  Connection conn = null;
	  
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update client set end=?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, end);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int updateScoreA(int score){
	  int rst = 0;
	  Connection conn = null;
	  
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update client set scorea=?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, score);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 
 public int updateScoreB(int score){
	  int rst = 0;
	  Connection conn = null;
	  
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection2();
	   String sql = "update client set scoreb=?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, score);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
}

