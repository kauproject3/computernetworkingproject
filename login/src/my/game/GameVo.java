package my.game;

public class GameVo {
	private int id1;
	private int id2;
	private int id3;
	private int id4;
	private int id5;
	private int id6;
	private int id7;
	private int id8;
	private int id9;
	private boolean isSet;
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	private int end;
	
	private int current1;
	private int current2;
	private int current3;
	private int current4;
	private int current5;
	private int current6;
	
	private int num;
	private int scorea;
	
	public int getScorea() {
		return scorea;
	}
	public void setScorea(int scorea) {
		this.scorea = scorea;
	}
	public int getScoreb() {
		return scoreb;
	}
	public void setScoreb(int scoreb) {
		this.scoreb = scoreb;
	}
	private int scoreb;
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public boolean isSet() {
		return isSet;
	}
	public void setSet(boolean isSet) {
		this.isSet = isSet;
	}
	public int getId1() {
		return id1;
	}
	public void setId1(int id1) {
		this.id1 = id1;
	}
	public int getId2() {
		return id2;
	}
	public void setId2(int id2) {
		this.id2 = id2;
	}
	public int getId3() {
		return id3;
	}
	public void setId3(int id3) {
		this.id3 = id3;
	}
	public int getId4() {
		return id4;
	}
	public void setId4(int id4) {
		this.id4 = id4;
	}
	public int getId5() {
		return id5;
	}
	public void setId5(int id5) {
		this.id5 = id5;
	}
	public int getId6() {
		return id6;
	}
	public void setId6(int id6) {
		this.id6 = id6;
	}
	public int getId7() {
		return id7;
	}
	public void setId7(int id7) {
		this.id7 = id7;
	}
	public int getId8() {
		return id8;
	}
	public void setId8(int id8) {
		this.id8 = id8;
	}
	public int getId9() {
		return id9;
	}
	public void setId9(int id9) {
		this.id9 = id9;
	}
	public int getCurrent1() {
		return current1;
	}
	public void setCurrent1(int current1) {
		this.current1 = current1;
	}
	public int getCurrent2() {
		return current2;
	}
	public void setCurrent2(int current2) {
		this.current2 = current2;
	}
	public int getCurrent3() {
		return current3;
	}
	public void setCurrent3(int current3) {
		this.current3 = current3;
	}
	public int getCurrent4() {
		return current4;
	}
	public void setCurrent4(int current4) {
		this.current4 = current4;
	}
	public int getCurrent5() {
		return current5;
	}
	public void setCurrent5(int current5) {
		this.current5 = current5;
	}
	public int getCurrent6() {
		return current6;
	}
	public void setCurrent6(int current6) {
		this.current6 = current6;
	}
}
