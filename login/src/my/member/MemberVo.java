package my.member;

public class MemberVo {
	private String id;
	private String pass;
	private String name;
	private String gender;
	private int birth1;
	private int birth2;
	private int birth3;
	private int score;
	private int tag;
	private int priority;
	private int isturn;
	
	public int getIsturn() {
		return isturn;
	}

	public void setIsturn(int isturn) {
		this.isturn = isturn;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getBirth1() {
		return birth1;
	}

	public void setBirth1(int birth1) {
		this.birth1 = birth1;
	}

	public int getBirth2() {
		return birth2;
	}

	public void setBirth2(int birth2) {
		this.birth2 = birth2;
	}

	public int getBirth3() {
		return birth3;
	}

	public void setBirth3(int birth3) {
		this.birth3 = birth3;
	}

}
