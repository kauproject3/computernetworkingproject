package my.member;
 
import java.sql.Connection;
import java.sql.PreparedStatement;

import my.util.ConnUtil;

import java.sql.ResultSet;

public class MemberDao {
	
 public int insertMember(MemberVo vo){
  int rst = 0;
  Connection conn = null;
  PreparedStatement ps = null;
  try{
   conn = ConnUtil.getConnection();
   String sql = "insert into member values(null,?,?,?,?,?,?,?,sysdate(),0,0,0,0)";
   ps = conn.prepareStatement(sql);
   ps.setString(1, vo.getId());
   ps.setString(2, vo.getPass());
   ps.setString(3, vo.getName());
   ps.setString(4, vo.getGender());
   ps.setInt(5, vo.getBirth1());
   ps.setInt(6, vo.getBirth2());
   ps.setInt(7, vo.getBirth3());
   rst = ps.executeUpdate();
  }catch(Exception e){
   e.printStackTrace();
  }finally{
   ConnUtil.close(ps, conn);
  }
  return rst;
 }
 
 public int loginCheck(String id, String pass){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "select * from member where id=?";
	   ps = conn.prepareStatement(sql);
	   ps.setString(1, id);
	   rs = ps.executeQuery();
	   if(rs.next()){
	    String DBpass = rs.getString("pass");
	     if((DBpass.trim()).equals((pass.trim()))){
	      rst=2;  // 로그인성공
	     }else{
	      rst=1;   // 비밀번호 틀림
	     }
	    }  // 아이디 없음
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return rst;
	 }
 
 public MemberVo selectScore(String id){
	  MemberVo vo = new MemberVo();
	  Connection conn = null;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "select score from member where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setString(1, id);
	   rs = ps.executeQuery();
	   if(rs.next()){
	    vo.setScore(rs.getInt("score"));
	   }
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return vo;
	 }
 
 public MemberVo selectClient(String id){
	  MemberVo vo = new MemberVo();
	  Connection conn = null;
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "select priority,isturn from member where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setString(1, id);
	   rs = ps.executeQuery();
	   if(rs.next()){
	    vo.setPriority(rs.getInt("priority"));
	    vo.setIsturn(rs.getInt("isturn"));
	   }
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(rs, ps, conn);
	  }
	  return vo;
	 }
 
 
 
 
 
 public int updateScore(int score,String id){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "update member set score=? where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, score);
	   ps.setString(2, id);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int updateClient(int num,String id){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "update member set priority=? where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, num);
	   ps.setString(2, id);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int updateIsturn(int isturn,String id){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "update member set isturn=? where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setInt(1, isturn);
	   ps.setString(2, id);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 public int initScore(String id){
	  int rst = 0;
	  Connection conn = null;
	  PreparedStatement ps = null;
	  try{
	   conn = ConnUtil.getConnection();
	   String sql = "update member set score=0 where id = ?";
	   ps = conn.prepareStatement(sql);
	   ps.setString(1, id);
	rst = ps.executeUpdate();
	  }catch(Exception e){
	   e.printStackTrace();
	  }finally{
	   ConnUtil.close(ps, conn);
	  }
	  return rst;
	 }
 
 
}