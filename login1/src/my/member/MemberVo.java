package my.member;

public class MemberVo {
	private String id;
	private String pass;
	private String name;
	private String gender;
	private int birth1;
	private int birth2;
	private int birth3;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getBirth1() {
		return birth1;
	}

	public void setBirth1(int birth1) {
		this.birth1 = birth1;
	}

	public int getBirth2() {
		return birth2;
	}

	public void setBirth2(int birth2) {
		this.birth2 = birth2;
	}

	public int getBirth3() {
		return birth3;
	}

	public void setBirth3(int birth3) {
		this.birth3 = birth3;
	}

}
