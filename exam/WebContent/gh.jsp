<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%!
	/*******************DATA*********************/
	int imgId[] = new int[9];					// 임의로 뽑은 9개의 이미지 ID
	String imgName[] = new String[9];			// ID에 따른 이미지의 버튼설정 string
	boolean isSet = false;						// 새로운 게임이 시작 될 때만 ID를 새로 뽑게하기 위한 bool값
	int table[] = {0,0,0,0,0,0};					// 정답을 찾아서 저장할 배열

	/********************************************/
	/*******************METHOD*******************/
	
	// 메서드 안에서만 사용할 DATA
	int existTag[] = new int[27];	// ID를 뽑을 때 중복을 없애기 위해 사용
	// method Rand()
	public void imgRand(){		// 랜덤으로 이미지 ID를 만들고 그에 해당하는 버튼 설정 값 할당
		for(int h=0;h<27;h++){		// 아무 것도 뽑지 않은 상태로 초기화	
			existTag[h]=0;
		}
		for(int i=0;i<9;i++){		// 0-26 까지의 정수 중 겹치지 않는 9개를 뽑을 때까지 계속 뽑는다.
			do{
				imgId[i] =(int)(Math.random()*27);
			}while(existTag[imgId[i]]!=0);			// 겹치면 다시
			existTag[imgId[i]]=1;					// 뽑은 녀석은 태그에 체크한다.
			imgId[i] =(1+imgId[i]/9)*100+(1+(imgId[i]%9)/3)*10+ (imgId[i]%9)%3+1;	// 0-26정수에 match되는 ID부여
		}
		for(int j=0;j<9;j++){		// ID에 해당하는 설정을 버튼에 넣어주기 위해 각각에 해당하는 String 저장
			imgName[j] =String.format("\"background:URL(C:/Users/seil/git/computernetworkingproject/exam/WebContent/images/%d.png);border:0px;width:200px; height:200px;cursor:pointer;\"",imgId[j]);
		}
		isSet=true;					// 랜덤으로 뽑으면 다음 판에 되기 전 까지는 다시 실행 되지 않게 한다.
	}

	// 메서드 안에서만 사용할 DATA
	int idx;	// 정답 테이블에 저장할 때 사용될 index
	int numA;	// 정답의 갯수
	int sum=0;	// ID의 합의 값 -> 정답 판별의 기준
	// metod answerTable()
	public void answerTable(){
		numA=0;
		idx=0;
		for(int i=0;i<7;i++){			// 모든 84가지(9개 중 3개) 조합을 검사 한다.
			for(int j=i+1;j<8;j++){
				for(int k=j+1;k<9;k++){
					sum=imgId[i]+imgId[j]+imgId[k];		// ID 더함
					// ID의 합의 모든 자리수가 3으로 나뉘는 경우에 한하여 정답이다.
					if((sum/100)%3==0&&(sum/10)%3==0&&sum%3==0){	
						table[idx] = 100*i+10*j+k;	// 정답 저장, index 증가
						numA++;	idx++;						// 정답의 수 증가
					}
				}
			}
		} 

	}


	/********************************************/
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript">
	<!--
		/*******************DATA*********************/
		var Bpick=[0,0,0];
		var left = 10;
		var answer = 0;
		
		/********************************************/
		/*******************METHOD*******************/

		// 메서드 안에서만 사용할 DATA
		var rear =0;	//
		var overlap;
		// method pick()
		function pick(BN){	// Button pick을 배열에 저장하는 메서드
			overlap = false;	// 중복이 없다고 초기화 해준다.
			for(i=0;i<3;i++) if(BN==Bpick[i]) overlap=true;		// 검사해서 중복이 있으면 true
			if(!overlap){		// 중복이 없다면
				Bpick[rear] = BN;	// 가장 마지막에 button num을 저장한다.
				rear = (rear+1)%3;	// 원형으로 돌아가면서 저장
			}
		}

		// 메서드 안에서만 사용할 DATA
		var idx;
		// method reArrange()
		function reArrange(){	// pick한 배열을 작은 순서대로 배열하여 3자리 숫자로 저장하는 메서드
			idx = 0;				// 배열에서 가장 작은 요소의 index 
			for(i=1;i<3;i++) if(Bpick[idx]>Bpick[i]) idx=i;	// 
			if(Bpick[(idx+1)%3]>Bpick[(idx+2)%3]){		// 나머지 두 요소 중 작은 것이 앞으로 가도록 정수 저장
				answer=Bpick[idx]*100+Bpick[(idx+2)%3]*10+Bpick[(idx+1)%3];
			}
			else{
				answer=Bpick[idx]*100+Bpick[(idx+1)%3]*10+Bpick[(idx+2)%3];
			}
		}

		// method reset()
		function reset(){		// 선택한 button pick 초기화하는 메서드
			for(i=0;i<3;i++) Bpick[i]=0;
			rear=0;					// 저장 위치도 초기화한다.
		}

		// method realTimeout()
		function realTimeout(){		// 일정 시간 간격으로 변경된 스크립트 값을 화면에 보여주는 메서드
			document.rtcForm.rtcInput.value = left;			// 남은 시간 
			document.rtcForm.c1.value = Bpick[0];			// 선택한 버튼 0
			document.rtcForm.c2.value = Bpick[1];			// 선택한 버튼 1
			document.rtcForm.c3.value = Bpick[2];			// 선택한 버튼 2
			reArrange();									// 선택한 버튼을 정렬하여 정수 값 얻는다.
			if(left==0){			// 시간이 끝난 경우
				document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
				document.sendForm.checkn.value = 2;
				document.sendForm.action = "index.jsp";		// post할 위치 지정
				document.sendForm.submit();					// post!!
			}
			left=left-0.1;									// 시간 감소
			setTimeout("realTimeout()", 100);				// 100 밀리 세컨드 단위로 최신화
		}
		
		  function setList(a) {
				if(a==0){
			  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
			  	document.sendForm.checkn.value = a;
				document.sendForm.action = "index.jsp";		// post할 위치 지정				
				document.sendForm.submit();	
				}
				else if(a==1){
				  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
				  	document.sendForm.checkn.value = a;
					document.sendForm.action = "index.jsp";		// post할 위치 지정
					document.sendForm.submit();	
					}
				else if(a==2){
				  	document.sendForm.Ainput.value = answer;	// post할 answer의 값 저장
				  	document.sendForm.checkn.value = a;
					document.sendForm.action = "index.jsp";		// post할 위치 지정
					document.sendForm.submit();	
					}
				}

			 

		/********************************************/
	-->
	</SCRIPT>
<title> GH game </title>
</head>
<body onload="realTimeout()">	
	<!--  realTimeout이 적용되는 body, rctForm을 사용하는 부분은 100밀리 세컨드마다 최신화 -->

	<%if(!isSet) imgRand();%>	<!-- 초기화가 필요할 경우(set이 안 된 경우) 랜덤으로 pick 한다. -->
	<%answerTable();%>			<!-- 랜덤에 맞춰 정답 테이블 작성 -->

	<!-- 랜덤으로 결정된 설정(imgName)에 따른 이미지를 갖는 버튼 배열 -->
	<button type="button" title="test" style=<%=imgName[0]%> onclick="pick(1)"></button>
	<button type="button" title="test" style=<%=imgName[1]%> onclick="pick(2)"></button>
	<button type="button" title="test" style=<%=imgName[2]%> onclick="pick(3)">></button><p>
	<button type="button" title="test" style=<%=imgName[3]%> onclick="pick(4)">></button>
	<button type="button" title="test" style=<%=imgName[4]%> onclick="pick(5)"></button>
	<button type="button" title="test" style=<%=imgName[5]%> onclick="pick(6)"></button><p>
	<button type="button" title="test" style=<%=imgName[6]%> onclick="pick(7)"></button>
	<button type="button" title="test" style=<%=imgName[7]%> onclick="pick(8)"></button>
	<button type="button" title="test" style=<%=imgName[8]%> onclick="pick(9)"></button><p>
	
	<!-- 실시간(100밀리 세컨 간격으로)으로 시간, pick한 버튼을 뿌려준다. -->
	<form name="rtcForm">
	<input type="text" name="rtcInput" size="2" readonly="readonly" /><p>
	<input type="text" name="c1" size="1" readonly="readonly"/>
	<input type="text" name="c2" size="1" readonly="readonly"/>
	<input type="text" name="c3" size="1" readonly="readonly"/>
	</form>
	<!-- reset 버튼 -->
	<button type="button" title="reset" onclick="reset()"></button><p>

	<!-- post할 form -->
	<form name="sendForm" method="post">
	<input type="hidden" name=Ainput value=''/>	
	<input type="hidden" name=checkn value=''/>			<!-- pick한 정답과 정답 테이블 post -->
	<input type="hidden" name="at0" value=<%=table[0]%>/>
	<input type="hidden" name="at1" value=<%=table[1]%>/>
	<input type="hidden" name="at2" value=<%=table[2]%>/>
	<input type="hidden" name="at3" value=<%=table[3]%>/>
	<input type="hidden" name="at4" value=<%=table[4]%>/>
	<input type="hidden" name="at5" value=<%=table[5]%>/>
	</form>

	
 <img src="images/rs.png"width="50"height="20"align="middle" onclick="reset()">
 	<form name="myFrm">
 <img src="images/gh.png"width="50"height="30"align="middle"onclick="setList(0)">	
 <img src="images/hap.png"width="50"height="30"align="middle"onclick="setList(1)">
 <img src="images/111.png"width="50"height="30"align="middle"onclick="setList(2)">
 </form>

</body>
</html>