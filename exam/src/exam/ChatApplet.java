package exam;
import java.awt.*;
import java.applet.Applet;
import java.applet.AppletContext ;
import java.net.*;
import java.awt.event.* ;
import java.io.*;
import java.lang.*;

 public class ChatApplet extends Applet implements Runnable, ActionListener {

    boolean is_connected = false;
    public String code ;
    public String location ;
    public String sabun ;
    Socket to_server;
    BufferedReader is;
    PrintWriter os;
    Thread thread;

    public Label lblName;
    public Label lblUsers;
    public Label lblCopyright;
    public TextField tfdName;
    public TextField tfdInput_box;
    public TextArea txaDisplay;
    public TextArea txaUser_list;

    public Button butConnect;
    public Button butQuit;

    public void init() {
        loadParameter() ;

        setForeground(Color.black);
        setBackground(new Color(230,228,219));
        setFont(new Font("TimesRoman",Font.PLAIN,12));

        lblName = new Label("��ȭ��",Label.CENTER);
        lblName.setFont(new Font("Curior",Font.PLAIN,12));
        lblUsers = new Label("����� ���",Label.CENTER);
        lblUsers.setFont(new Font("Curior",Font.PLAIN,12));
        lblCopyright = new Label("���ѻ�� ���̹���������",Label.CENTER);
        lblCopyright.setFont(new Font("Dialog",Font.PLAIN,12));
        
        tfdName = new TextField(10);
        tfdName.setForeground(Color.black);
        tfdName.setBackground(Color.white);
        tfdName.setFont(new Font("Curior",Font.PLAIN,14));
        tfdName.setText(code) ;
        tfdInput_box = new TextField(50);
        tfdInput_box.setForeground(new Color(38,0,72));
        tfdInput_box.setBackground(Color.white);
        tfdInput_box.setFont(new Font("TimesRoman",Font.PLAIN,14));
        tfdInput_box.addActionListener(this) ;
        
        butConnect = new Button("Connect");
        butConnect.addActionListener(this) ;
        butQuit = new Button("Quit");
        butQuit.addActionListener(this) ;
        
        txaDisplay = new TextArea(20,40);
        txaDisplay.setEditable(false);
        txaDisplay.setForeground(Color.black);
        txaDisplay.setBackground(Color.white);
        txaDisplay.setFont(new Font("Curior",Font.PLAIN,12));
        txaUser_list = new TextArea(10,10);
        txaUser_list.setEditable(false);
        txaUser_list.setForeground(Color.black);
        txaUser_list.setBackground(Color.white);
        txaUser_list.setFont(new Font("Dialog",Font.PLAIN,12));

        GridBagLayout gb = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        this.setLayout(gb);

        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        gb.setConstraints(lblName, c);
        add(lblName);

        gb.setConstraints(tfdName, c);
        add(tfdName);

        gb.setConstraints(butConnect, c);
        add(butConnect);

        gb.setConstraints(butQuit, c);
        add(butQuit);

        c.gridwidth = GridBagConstraints.REMAINDER;
        gb.setConstraints(lblUsers, c);
        add(lblUsers);

        c.gridwidth = GridBagConstraints.RELATIVE;
        c.gridheight = 3;
        gb.setConstraints(txaDisplay, c);
        add(txaDisplay);

        c.gridwidth = GridBagConstraints.REMAINDER;
        gb.setConstraints(txaUser_list, c);
        add(txaUser_list);

        c.gridwidth = GridBagConstraints.REMAINDER;
        gb.setConstraints(tfdInput_box, c);
        add(tfdInput_box);

        c.gridwidth = GridBagConstraints.REMAINDER;
        gb.setConstraints(lblCopyright, c);
        add(lblCopyright);
    }

    public void loadParameter() {
        String c = getParameter("tempCode") ;
        String l = getParameter("location") ;
        String s = getParameter("sabun") ; 

        if (c != null && l != null && s != null) {
            code     = c ;
            location = l ;
            sabun    = s ;
        } else if (c == null || l == null || s == null) {
            code     = "cyber" ;
            location = "1" ;
            sabun    = "12345678" ;
        }
    }

    public void start() {
        thread = new Thread(this,"main");
        thread.start();
    }

    public void run() {
        try {
            txaDisplay.append("��ȭ���� �Է��ϰ� Connect��ư�� Ŭ���ϸ� �����մϴ�.\n");
            while (true) {
                try {
                     thread.sleep(300);
                    } catch (InterruptedException e) {}
                if (!is_connected || is == null || os == null)
                    continue;

                    if (is.ready()) {
                        String text = is.readLine() + '\n';
                    if (text.startsWith("/name "))
                        write_list(text.substring(6));
                    else if (text.startsWith("/quit"))
                        write_list(text.substring(5));
                                    else
                        txaDisplay.append(text);
                    }
            }
        } catch (IOException e) {}
    }

    public void write_list(String list) {
        int i;
        int next;
        txaUser_list.setText("");

        for (i=0; i<list.length(); i++) {
            next = list.indexOf('|',i);
            if (next >= list.length() || next == -1)
                break;
            txaUser_list.append(list.substring(i,next) + '\n');
            i = next;
        }
    }
    
    public void actionPerformed(ActionEvent evt) {        
          if (!is_connected) {
              String text = tfdName.getText();
              int len = text.length();
              if (len != 0) {
                  txaDisplay.append("������ �Դϴ�...\n");
                  try {
                      try {
                          to_server = new Socket(getCodeBase().getHost(),4444);
                      } catch (UnknownHostException e) {
                          txaDisplay.append("��� �߻�!\n");
                      }
                      is = new BufferedReader(new InputStreamReader(to_server.getInputStream()));
                      os = new PrintWriter(new BufferedOutputStream(to_server.getOutputStream(),1024),false);
                     os.println("/name " + text  + " code " + code + " location " + location + " sabun " + sabun );
                     os.flush();
                  } catch (IOException e) {}
                  is_connected = true;
                  txaDisplay.append(text+" ���� �����ϼ̽��ϴ�.\n");
              }
          }

          if (is_connected) {
              if (os == null || is == null) {
                  txaDisplay.append("��� �߻�!\n");
              }
              String text = tfdInput_box.getText();
              int len = text.length();
              if (len != 0) {
                     tfdInput_box.setText("");
                     os.println(text);
                     os.flush();
              }
          }
  
          if ((evt.getActionCommand().equals("Quit")) && is_connected) {
              if (os == null || is == null) {
                  txaDisplay.append("��� �߻�!\n");
              }
              try {
                  os.println("/quit");
                  os.flush();
  
                  if (os != null)
                      os.close();
                  if (is != null)
                      is.close();
                  if (to_server != null)
                      to_server.close();
              } catch (IOException e) {
                  txaDisplay.append("��� �߻�!\n");
              }
              txaDisplay.append("������ ���� �Ǿ���ϴ�.\n");
              txaUser_list.setText("");
              is_connected = false;
          }
  
          if ((evt.getActionCommand().equals("Connect")) && !is_connected) {
              String text = tfdName.getText();
              int len = text.length();
              if (len != 0) {
                  txaDisplay.append("���� ���Դϴ�...\n");
                  try {
                       try {
                           to_server = new Socket(getCodeBase().getHost(),4444);
                       } catch (UnknownHostException e) {
                           txaDisplay.append("��� �߻�!\n");
                       }
  
                       is = new BufferedReader(new InputStreamReader(to_server.getInputStream()));
                       os = new PrintWriter(new BufferedOutputStream(to_server.getOutputStream(),1024),false);
                       os.println("/name " + text + " code " + code + " location " + location + " sabun " + sabun );
                       os.flush();
                  } catch (IOException e) {}
  
                  is_connected = true;
                  txaDisplay.append(text+" ���� ���� �ϼ̽��ϴ�.\n");
              }
          }
    }

    public void stop() {
        if (os != null) {
            os.println("/quit");
            os.flush();
        }
        try {
            if (os != null)
                os.close();
            if (is != null)
                is.close();
            if (to_server != null)
                to_server.close();
        } catch (IOException e) {
        txaDisplay.append("��� �߻�!\n");
        }
        txaUser_list.setText("");
        is_connected = false;
    }
}
