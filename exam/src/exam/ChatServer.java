package exam;
import java.io.* ;
import java.net.* ;
import java.lang.* ;
import java.util.* ;

class ChatService extends Thread {
   Socket xSocket = null ;
   ChatServer xServer = null ;

   // ������ ���κ� �ڷ�
   String name = new String("Guest");
   String code = "" ; //�������� ������� �ڵ�
   String location = "" ; //���� �� ä�ù� ��
   String sabun = "" ; // ������ ����ȣ
   String connectSecond = Long.toString(java.lang.System.currentTimeMillis()/1000) ;
   
   PrintWriter out = null ;
   BufferedReader in = null ;
   
   public ChatService(Socket sock, ChatServer srv) {
      super() ;
      xSocket = sock ;
      xServer = srv ;
   }

   public synchronized void sendMessage(String msg) {
      if (out != null) 
          out.println(msg) ;
   }

   public void run() {
      String line ;
      
      try {
           out = new PrintWriter(xSocket.getOutputStream(),true) ;
           in  = new BufferedReader(new InputStreamReader(xSocket.getInputStream())) ;
           
           xServer.addClient(this) ;
           
           while ((line = in.readLine()) != null) {
                  xServer.broadcast(line, this) ;
           }
      } catch (IOException ex) {
      } finally {
           try {
                xServer.removeClient(this) ;
                xServer.sendNames(code,location) ;
 
                if (out != null) 
                    out.close() ;
                if (in != null)
                    in.close() ;
                if (xSocket != null) 
                    xSocket.close() ;
           } catch (Exception ex2) {
           }
      }
   }
}

public class ChatServer {
   ServerSocket srvsocket = null ;
   Socket consocket = null ;
   int port = 4444 ;
   
   Vector vClients = new Vector() ;
   
   public void doJob() {
      try {
           srvsocket = new ServerSocket(port) ;
           
           while (true) {
                  consocket = srvsocket.accept() ;
                  
                  ChatService service = new ChatService(consocket, this) ;
                  service.start() ;
           }
      } catch (IOException ex) {
      } finally {
           try {
                if (srvsocket != null) 
                    srvsocket.close() ;
           } catch (Exception ex2) {}
      }
   }

   public synchronized void broadcast(String msg, ChatService cs) {
      String[] results = {"","","",""} ;
      int index = vClients.indexOf(cs) ;

      if (msg.startsWith("/name ")) {
          results = makeTokenizeString(msg) ;
          cs.name = results[0] ;
          cs.code = results[1] ;
          cs.location = results[2] ;
          cs.sabun = results[3] ;
          cs.sendMessage("��ȭ���� " + results[0] + "���� ����Ǿ���ϴ�.") ;
          
          vClients.set(index, cs) ;

          sendNames(cs.code, cs.location) ;
      } else if (msg.equals("/quit")) {
          writeAll(cs.name + "���� �����ϼ̽��ϴ�.", cs.code, cs.location) ;
      } else { // ��ȭ������ ���
          writeAll(cs.name + "> " + msg , cs.code, cs.location) ;
      }
   }

   public void writeAll(String msg, String code, String location) {
      for(int i=0 ; i < vClients.size() ; i++) {
          ChatService client = ((ChatService) vClients.elementAt(i)) ;

          if (code.equals(client.code) && location.equals(client.location))
              client.sendMessage(msg) ;
      }
   }

   public synchronized void addClient(ChatService cs) {
      vClients.add(cs) ;
   }
   
   public synchronized void removeClient(ChatService cs) {
      vClients.remove(cs) ;
   }

   public void sendNames(String code, String location) {
      String names = new String("/name ") ;
      
      for(int i=0 ; i < vClients.size() ; i++) {
          ChatService client = ((ChatService) vClients.elementAt(i)) ;

          if (code.equals(client.code) && location.equals(client.location))
              names = names + client.name + "|" ;         
      }

      writeAll(names,code,location) ;
   }

   public String[] makeTokenizeString(String str) {
      String[] result = {"","","", ""} ;
      StringTokenizer st = new StringTokenizer(str) ;
      String tempElement = new String() ;
      int i = 1 ;
      
      while (st.hasMoreTokens()) {
             tempElement = st.nextToken() ;
             
             switch (i) {
                case 2 :
                   result[0] = tempElement ;
                   break ;
                case 4 :
                   result[1] = tempElement ;
                   break ;
                case 6 :
                   result[2] = tempElement ;
                   break ;
                case 8 :
                   result[3] = tempElement ;
                   break ;
             }
             
             i++ ;
      }
      
      return result ;
   }

   public static void main(String args[]) {
      ChatServer server = new ChatServer() ;
      server.doJob() ;
   }
}
